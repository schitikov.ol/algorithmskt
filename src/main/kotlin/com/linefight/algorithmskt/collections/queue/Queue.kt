package com.linefight.algorithmskt.collections.queue

interface Queue<T> : Iterable<T> {
    val size: Int

    fun enqueue(item: T)

    fun dequeue(): T

    fun isEmpty(): Boolean
}