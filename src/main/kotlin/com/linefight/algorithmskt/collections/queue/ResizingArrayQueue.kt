package com.linefight.algorithmskt.collections.queue

class ResizingArrayQueue<T> : Queue<T> {

    @Suppress("UNCHECKED_CAST")
    private var items: Array<T?> = arrayOfNulls<Any>(1) as Array<T?>

    private var firstPos: Int = 0
    private var lastPos: Int = -1

    override var size: Int = 0
        private set

    override fun enqueue(item: T) {
        if (size == items.size) resize(2 * items.size)

        items[++lastPos] = item
        size++
    }

    override fun dequeue(): T {
        if (isEmpty()) throw NoSuchElementException()

        val item = items[firstPos++]
        items[firstPos - 1] = null
        size--

        if (size == items.size / 4) resize(items.size / 2)

        return item!!
    }

    override fun isEmpty(): Boolean = size == 0

    override fun iterator(): Iterator<T> = object : Iterator<T> {
        var pos: Int = firstPos

        override fun hasNext(): Boolean = pos <= lastPos

        override fun next(): T {
            if (!hasNext()) throw NoSuchElementException()

            return items[pos++]!!
        }
    }

    private fun resize(max: Int) {
        @Suppress("UNCHECKED_CAST")
        val temp = arrayOfNulls<Any>(max) as Array<T?>

        items.copyInto(temp, destinationOffset = 0, startIndex = firstPos, endIndex = lastPos + 1)

        firstPos = 0
        lastPos = size - 1

        items = temp
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ResizingArrayQueue<*>) return false

        if (size != other.size) return false

        @Suppress("UNCHECKED_CAST")
        val castedOther = other as ResizingArrayQueue<T>

        if (!items.contentDeepEquals(castedOther.items)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = items.contentDeepHashCode()
        result = 31 * result + size
        return result
    }
}