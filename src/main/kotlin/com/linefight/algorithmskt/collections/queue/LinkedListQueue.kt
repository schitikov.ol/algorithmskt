package com.linefight.algorithmskt.collections.queue

import com.linefight.algorithmskt.collections.Node

class LinkedListQueue<T> : Queue<T> {

    private var first: Node<T>? = null
    private var last: Node<T>? = null

    override var size: Int = 0
        private set

    override fun enqueue(item: T) {
        val node = Node(item)
        last?.next = node
        last = node

        if (isEmpty()) first = node
        size++
    }

    override fun dequeue(): T {
        if (isEmpty()) throw NoSuchElementException()

        val item: T

        with(first!!) {
            item = this.item
            first = this.next
        }
        size--

        return item
    }

    override fun isEmpty(): Boolean = size == 0

    override fun iterator(): Iterator<T> = object : Iterator<T> {
        var currentNode: Node<T>? = first

        override fun hasNext(): Boolean = currentNode != null

        override fun next(): T {
            if (!hasNext()) throw NoSuchElementException()

            val item: T

            with(currentNode!!) {
                item = this.item
                currentNode = this.next
            }

            return item
        }

    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LinkedListQueue<*>) return false

        if (size != other.size) return false
        if (first != other.first) return false

        return true
    }

    override fun hashCode(): Int {
        var result = first?.hashCode() ?: 0
        result = 31 * result + size
        return result
    }
}