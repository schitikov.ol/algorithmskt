package com.linefight.algorithmskt.collections

data class Node<T>(val item: T, var next: Node<T>? = null)