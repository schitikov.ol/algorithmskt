package com.linefight.algorithmskt.collections.stack

interface Stack<T> : Iterable<T> {
    val size: Int

    fun push(item: T)

    fun pop(): T

    fun isEmpty(): Boolean
}