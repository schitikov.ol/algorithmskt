package com.linefight.algorithmskt.collections.stack

class ResizingArrayStack<T> : Stack<T> {

    @Suppress("UNCHECKED_CAST")
    private var items: Array<T?> = arrayOfNulls<Any>(1) as Array<T?>

    override var size: Int = 0
        private set

    override fun push(item: T) {
        if (items.size == size) resize(2 * items.size)

        items[size++] = item
    }

    override fun pop(): T {
        if (isEmpty()) throw NoSuchElementException()

        val item = items[--size]

        items[size] = null

        if (size == items.size / 4) resize(items.size / 2)

        return item!!
    }

    override fun isEmpty(): Boolean = size == 0

    override fun iterator(): Iterator<T> {
        return object : Iterator<T> {
            private var pos: Int = size

            override fun hasNext(): Boolean = pos > 0

            override fun next(): T = if (hasNext()) items[--pos]!! else throw NoSuchElementException()

        }
    }

    private fun resize(max: Int) {
        items = items.copyOf(max)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ResizingArrayStack<*>) return false

        if (size != other.size) return false

        @Suppress("UNCHECKED_CAST")
        val castedOther = other as ResizingArrayStack<T?>

        if (!items.contentDeepEquals(castedOther.items)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = items.contentDeepHashCode()
        result = 31 * result + size
        return result
    }
}