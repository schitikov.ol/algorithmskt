package com.linefight.algorithmskt.collections.unionfind

import com.linefight.algorithmskt.collections.stack.ResizingArrayStack

class QuickUnionUnionFind(numberOfComponents: Int) : UnionFind {
    override var count: Int = numberOfComponents
        private set

    private val id: IntArray = IntArray(numberOfComponents) { it }
    private val sz: IntArray = IntArray(numberOfComponents) { 1 }

    override fun union(p: Int, q: Int) {
        val pRoot = find(p)
        val qRoot = find(q)

        if (pRoot == qRoot) return

        //connect to smaller root
        if (sz[pRoot] < sz[qRoot]) {
            id[pRoot] = qRoot
            sz[qRoot] += sz[pRoot]
        } else {
            id[qRoot] = pRoot
            sz[pRoot] += sz[qRoot]
        }

        count--
    }

    override fun find(p: Int): Int {
        val pathToRoot = ResizingArrayStack<Int>()

        var q = p
        while (id[q] != q) {
            pathToRoot.push(q)

            //if q's parent is not root then subtract q's size from q's parent size due to tree rebuilding during to path compression
            if (id[q] != id[id[q]])
                sz[id[q]] -= sz[q]

            q = id[q]
        }

        //path compression
        for (index in pathToRoot) id[index] = q

        return q
    }
}