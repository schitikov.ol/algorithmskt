package com.linefight.algorithmskt.collections.unionfind

interface UnionFind {

    val count: Int

    fun union(p: Int, q: Int)

    fun find(p: Int): Int

    fun connected(p: Int, q: Int): Boolean = find(p) == find(q)
}