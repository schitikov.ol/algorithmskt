package com.linefight.algorithmskt.collections.unionfind

class QuickFindUnionFind(numberOfComponents: Int) : UnionFind {

    override var count = numberOfComponents
        private set

    private val id: IntArray = IntArray(numberOfComponents) { it }

    override fun union(p: Int, q: Int) {
        val pId = find(p)
        val qId = find(q)

        if (pId == qId) return

        for (i in id.indices) if (find(i) == pId) id[i] = qId

        count--
    }

    override fun find(p: Int): Int = id[p]
}