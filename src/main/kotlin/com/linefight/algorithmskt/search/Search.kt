package com.linefight.algorithmskt.search

fun <T : Comparable<T>> List<T>.binarySearch(key: T): Int = doBinarySearch(key, size, this::get)

fun <T : Comparable<T>> Array<out T>.binarySearch(key: T): Int = doBinarySearch(key, size, this::get)

private inline fun <T : Comparable<T>> doBinarySearch(key: T, collectionSize: Int, accessByIndex: (Int) -> T): Int {
    var lo = 0
    var hi = collectionSize - 1

    while (lo <= hi) {
        val mid = (hi - lo) / 2 + lo

        when {
            key > accessByIndex(mid) -> lo = mid + 1
            key < accessByIndex(mid) -> hi = mid - 1
            else -> return mid
        }
    }

    return -1
}