package com.linefight.algorithmskt.collections.queue

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals

internal abstract class QueueTest {
    abstract fun createInstance(): Queue<Int>

    @Test
    fun testSeveralEnqueueAndDequeue() {
        val queue = createInstance()

        val itemsToEnqueue = generateSequence { Random.nextInt() }.take(10).toList()

        for (item in itemsToEnqueue) {
            queue.enqueue(item)
        }

        assertEquals(itemsToEnqueue.size, queue.size)

        assertEquals(itemsToEnqueue[0], queue.dequeue())
        assertEquals(itemsToEnqueue[1], queue.dequeue())
        assertEquals(itemsToEnqueue[2], queue.dequeue())

        assertEquals(itemsToEnqueue.size - 3, queue.size)

        for (i in itemsToEnqueue.indices.drop(3)) {
            assertEquals(itemsToEnqueue[i], queue.dequeue())
        }
    }

    @Test
    fun testIsEmpty() {
        val queue = createInstance()

        assertTrue(queue.isEmpty())

        for (item in 0..9) {
            queue.enqueue(item)
        }

        assertFalse(queue.isEmpty())

        for (item in 0..9) {
            queue.dequeue()
        }

        assertTrue(queue.isEmpty())
    }

    @Test
    fun testIterator() {
        val queue = createInstance()

        val itemsToEnqueue = generateSequence { Random.nextInt() }.take(10).toList()

        for (item in itemsToEnqueue) {
            queue.enqueue(item)
        }

        for ((index, item) in queue.withIndex()) {
            assertEquals(itemsToEnqueue[index], item)
        }

        assertEquals(itemsToEnqueue.size, queue.size)
    }

    @Test
    fun testDequeueOnEmptyQueue() {
        val queue = createInstance()

        assertThrows<NoSuchElementException> { queue.dequeue() }
    }

    @Test
    fun testIteratorNextOnEmptyQueue() {
        val iter = createInstance().iterator()

        assertFalse(iter.hasNext())
        assertThrows<NoSuchElementException> { iter.next() }
    }

    @Test
    fun testEqualsAndHashCode() {
        val itemsToEnqueue = generateSequence { Random.nextInt() }.take(10).toList()

        val queueA = createInstance()
        val queueB = createInstance()
        val queueC = createInstance()

        assertEquals(queueA, queueB)
        assertEquals(queueB, queueC)
        assertEquals(queueA, queueC)

        assertEquals(queueA.hashCode(), queueB.hashCode())
        assertEquals(queueB.hashCode(), queueC.hashCode())
        assertEquals(queueA.hashCode(), queueC.hashCode())

        for (item in itemsToEnqueue) {
            queueA.enqueue(item)
            queueB.enqueue(item)
            queueC.enqueue(item)
        }
        queueC.dequeue()

        assertEquals(queueA, queueB)
        assertNotEquals(queueB, queueC)
        assertNotEquals(queueA, queueC)

        assertEquals(queueA.hashCode(), queueB.hashCode())
    }
}

internal class ResizingArrayQueueTest : QueueTest() {
    override fun createInstance(): Queue<Int> = ResizingArrayQueue()
}

internal class LinkedListQueueTest : QueueTest() {
    override fun createInstance(): Queue<Int> = LinkedListQueue()
}