package com.linefight.algorithmskt.collections.stack

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

internal abstract class StackTest {

    abstract fun createInstance(): Stack<Int>

    @Test
    fun testSeveralPushAndPop() {
        val stack = createInstance()

        val itemsToPush = generateSequence { Random.nextInt() }.take(10).toList()

        for (item in itemsToPush) {
            stack.push(item)
        }

        assertEquals(itemsToPush.size, stack.size)

        assertEquals(itemsToPush[itemsToPush.size - 1], stack.pop())
        assertEquals(itemsToPush[itemsToPush.size - 2], stack.pop())
        assertEquals(itemsToPush[itemsToPush.size - 3], stack.pop())

        assertEquals(itemsToPush.size - 3, stack.size)

        for (i in itemsToPush.indices.reversed().drop(3)) {
            assertEquals(itemsToPush[i], stack.pop())
        }
    }

    @Test
    fun testIterator() {
        val stack = createInstance()

        val itemsToPush = generateSequence { Random.nextInt() }.take(10).toList()

        for (item in itemsToPush) {
            stack.push(item)
        }

        for ((index, item) in stack.withIndex()) {
            assertEquals(item, itemsToPush[(stack.size - 1) - index])
        }

        assertEquals(stack.size, itemsToPush.size)
    }

    @Test
    fun testIsEmpty() {
        val stack = createInstance()

        assertTrue(stack.isEmpty())

        for (item in 0..9) {
            stack.push(item)
        }

        assertFalse(stack.isEmpty())

        for (item in 0..9) {
            stack.pop()
        }

        assertTrue(stack.isEmpty())
    }

    @Test
    fun testPopOnEmptyStack() {
        val stack = createInstance()

        assertThrows<NoSuchElementException> { stack.pop() }
    }

    @Test
    fun testIteratorNextOnEmptyStack() {
        val stack = createInstance()

        val iter = stack.iterator()

        assertFalse(iter.hasNext())
        assertThrows<NoSuchElementException> { iter.next() }
    }

    @Test
    fun testEqualsAndHashCode() {
        val itemsToPush = generateSequence { Random.nextInt() }.take(10).toList()

        val stackA = createInstance()
        val stackB = createInstance()
        val stackC = createInstance()

        assertEquals(stackA, stackB)
        assertEquals(stackB, stackC)
        assertEquals(stackA, stackC)

        assertEquals(stackA.hashCode(), stackB.hashCode())
        assertEquals(stackB.hashCode(), stackC.hashCode())
        assertEquals(stackA.hashCode(), stackC.hashCode())

        for (item in itemsToPush) {
            stackA.push(item)
            stackB.push(item)
            stackC.push(item)
        }
        stackC.pop()

        assertEquals(stackA, stackB)
        assertNotEquals(stackB, stackC)
        assertNotEquals(stackA, stackC)

        assertEquals(stackA.hashCode(), stackB.hashCode())
    }
}

internal class ResizingArrayStackTest : StackTest() {
    override fun createInstance(): Stack<Int> = ResizingArrayStack()
}

internal class LinkedListStackTest : StackTest() {
    override fun createInstance(): Stack<Int> = LinkedListStack()
}