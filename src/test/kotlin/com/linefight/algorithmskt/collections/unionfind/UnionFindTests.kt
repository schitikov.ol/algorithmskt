package com.linefight.algorithmskt.collections.unionfind

import org.junit.jupiter.api.Assertions.assertTimeout
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.time.Duration
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals

internal abstract class UnionFindTest {

    abstract fun createInstance(numberOfComponents: Int): UnionFind

    @Test
    fun testUnionFind() {
        val uf = createInstance(5)

        assertNotEquals(uf.find(0), uf.find(1))
        uf.union(0, 1)
        assertEquals(uf.find(0), uf.find(1))

        assertNotEquals(uf.find(2), uf.find(0))
        assertNotEquals(uf.find(2), uf.find(1))
        uf.union(0, 2)
        assertEquals(uf.find(2), uf.find(0))
        assertEquals(uf.find(2), uf.find(1))

        uf.union(3, 4)
        uf.union(3, 0)

        assertEquals(uf.find(3), uf.find(0))
        assertEquals(uf.find(4), uf.find(0))
    }

    @Test
    fun testConnectAll() {
        val uf = createInstance(100)

        for (i in 0 until 99) {
            uf.union(i, i + 1)
        }

        assertEquals(1, uf.count)
    }

    @Test
    fun testRandomUnionsUntilAllConnected() {
        val uf = createInstance(100)

        assertTimeout(Duration.ofSeconds(5)) {
            while (uf.count != 1) {
                val p = Random.nextInt(100)
                val q = Random.nextInt(100)

                uf.union(p, q)

                assertTrue(uf.connected(p, q))
            }
        }
    }

    @Test
    fun testConnected() {
        val uf = createInstance(2)

        assertFalse(uf.connected(0, 1))
        uf.union(0, 1)
        assertTrue(uf.connected(0, 1))
    }

    @Test
    fun testCount() {
        val uf = createInstance(4)

        assertEquals(4, uf.count)
        uf.union(0, 1)
        assertEquals(3, uf.count)
        uf.union(2, 3)
        assertEquals(2, uf.count)
        uf.union(0, 3)
        assertEquals(1, uf.count)
    }
}

internal class QuickFindUnionFindTest : UnionFindTest() {
    override fun createInstance(numberOfComponents: Int): UnionFind = QuickFindUnionFind(numberOfComponents)
}

internal class QuickUnionUnionFindTest : UnionFindTest() {
    override fun createInstance(numberOfComponents: Int): UnionFind = QuickUnionUnionFind(numberOfComponents)
}