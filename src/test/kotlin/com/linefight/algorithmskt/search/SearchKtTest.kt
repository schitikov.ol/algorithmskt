package com.linefight.algorithmskt.search

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class SearchKtTest {

    @Test
    fun binarySearch_keyIsPresent() {
        val key = 3
        val list = listOf(1, 2, 3, 4, 5, 6, 7)

        assertEquals(list.indexOf(key), list.binarySearch(key))
    }

    @Test
    fun binarySearch_keyIsNotPresent() {
        val key = 5
        val list = listOf(1, 2, 3, 4, 6, 7)

        assertEquals(-1, list.binarySearch(key))
    }

    @Test
    fun binarySearch_singletonList_keyIsPresent() {
        val key = 1
        val list = listOf(1)

        assertEquals(0, list.binarySearch(key))
    }

    @Test
    fun binarySearch_singletonList_keyIsNotPresent() {
        val key = 0
        val list = listOf(1)

        assertEquals(-1, list.binarySearch(key))
    }

    @Test
    fun binarySearch_emptyList() {
        val key = 0
        val list = emptyList<Int>()

        assertEquals(-1, list.binarySearch(key))
    }
}